package com.mycompany.myapp.domain.enumeration;

/**
 * The ValidationDC enumeration.
 */
public enum ValidationDC {
    VALIDE, INVALIDE, TRAITEMENT
}
