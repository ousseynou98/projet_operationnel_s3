package com.mycompany.myapp.domain.enumeration;

/**
 * The ValidationR enumeration.
 */
public enum ValidationR {
    VALIDE, INVALIDE, TRAITEMENT
}
