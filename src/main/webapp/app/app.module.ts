import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { AccordSharedModule } from 'app/shared/shared.module';
import { AccordCoreModule } from 'app/core/core.module';
import { AccordAppRoutingModule } from './app-routing.module';
import { AccordHomeModule } from './home/home.module';
import { AccordEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    AccordSharedModule,
    AccordCoreModule,
    AccordHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    AccordEntityModule,
    AccordAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class AccordAppModule {}
