import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IAccord, Accord } from 'app/shared/model/accord.model';
import { AccordService } from './accord.service';

@Component({
  selector: 'jhi-accord-update',
  templateUrl: './accord-update.component.html',
})
export class AccordUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    partenaire: [],
    domaine: [],
    description: [],
    date: [],
    dure: [],
    zone: [],
    type: [],
    nature: [],
    validationDircoop: [],
    validationRecteur: [],
  });

  constructor(protected accordService: AccordService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accord }) => {
      if (!accord.id) {
        const today = moment().startOf('day');
        accord.date = today;
      }

      this.updateForm(accord);
    });
  }

  updateForm(accord: IAccord): void {
    this.editForm.patchValue({
      id: accord.id,
      partenaire: accord.partenaire,
      domaine: accord.domaine,
      description: accord.description,
      date: accord.date ? accord.date.format(DATE_TIME_FORMAT) : null,
      dure: accord.dure,
      zone: accord.zone,
      type: accord.type,
      nature: accord.nature,
      validationDircoop: accord.validationDircoop,
      validationRecteur: accord.validationRecteur,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const accord = this.createFromForm();
    if (accord.id !== undefined) {
      this.subscribeToSaveResponse(this.accordService.update(accord));
    } else {
      this.subscribeToSaveResponse(this.accordService.create(accord));
    }
  }

  private createFromForm(): IAccord {
    return {
      ...new Accord(),
      id: this.editForm.get(['id'])!.value,
      partenaire: this.editForm.get(['partenaire'])!.value,
      domaine: this.editForm.get(['domaine'])!.value,
      description: this.editForm.get(['description'])!.value,
      date: this.editForm.get(['date'])!.value ? moment(this.editForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined,
      dure: this.editForm.get(['dure'])!.value,
      zone: this.editForm.get(['zone'])!.value,
      type: this.editForm.get(['type'])!.value,
      nature: this.editForm.get(['nature'])!.value,
      validationDircoop: this.editForm.get(['validationDircoop'])!.value,
      validationRecteur: this.editForm.get(['validationRecteur'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAccord>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
