import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAccord } from 'app/shared/model/accord.model';
import { AccordService } from './accord.service';
import { AccordDeleteDialogComponent } from './accord-delete-dialog.component';

@Component({
  selector: 'jhi-accord',
  templateUrl: './accord.component.html',
})
export class AccordComponent implements OnInit, OnDestroy {
  accords?: IAccord[];
  eventSubscriber?: Subscription;

  constructor(protected accordService: AccordService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.accordService.query().subscribe((res: HttpResponse<IAccord[]>) => (this.accords = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAccords();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAccord): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAccords(): void {
    this.eventSubscriber = this.eventManager.subscribe('accordListModification', () => this.loadAll());
  }

  delete(accord: IAccord): void {
    const modalRef = this.modalService.open(AccordDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.accord = accord;
  }
}
