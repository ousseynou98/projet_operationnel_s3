import { Moment } from 'moment';
import { ValidationDC } from 'app/shared/model/enumerations/validation-dc.model';
import { ValidationR } from 'app/shared/model/enumerations/validation-r.model';

export interface IAccord {
  id?: number;
  partenaire?: string;
  domaine?: string;
  description?: string;
  date?: Moment;
  dure?: string;
  zone?: string;
  type?: string;
  nature?: string;
  validationDircoop?: ValidationDC;
  validationRecteur?: ValidationR;
}

export class Accord implements IAccord {
  constructor(
    public id?: number,
    public partenaire?: string,
    public domaine?: string,
    public description?: string,
    public date?: Moment,
    public dure?: string,
    public zone?: string,
    public type?: string,
    public nature?: string,
    public validationDircoop?: ValidationDC,
    public validationRecteur?: ValidationR
  ) {}
}
