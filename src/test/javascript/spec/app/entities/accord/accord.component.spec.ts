import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AccordTestModule } from '../../../test.module';
import { AccordComponent } from 'app/entities/accord/accord.component';
import { AccordService } from 'app/entities/accord/accord.service';
import { Accord } from 'app/shared/model/accord.model';

describe('Component Tests', () => {
  describe('Accord Management Component', () => {
    let comp: AccordComponent;
    let fixture: ComponentFixture<AccordComponent>;
    let service: AccordService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AccordTestModule],
        declarations: [AccordComponent],
      })
        .overrideTemplate(AccordComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AccordComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AccordService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Accord(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.accords && comp.accords[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
